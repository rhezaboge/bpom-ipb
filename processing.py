import pandas as pd
import json
import numpy as np
from nltk.tokenize import sent_tokenize, word_tokenize, RegexpTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from datetime import datetime




gv_folder='17-10-19/'                  #folder yang berisi folder-folder jenis data baru
gv_tipe_data = 'obat'                  #jenis data dan nama folder yang harus ada didalam gv_folder
gv_folder_frontend = 'data_2019_10_17' #folder yang akan dibaca oleh javascript 
gv_range = 501                         #jumlah data json yang ada pada folder gv_tipe_data
gv_sold = 500                          #data yang dianalisis (penjualan diatas angka xxx)






now = datetime.now()
gv_dt_string = now.strftime("%d_%m_%Y__%H_%M_%S")

frames = []
stopWords = ["600","400","10","strip","isi","bpom","caps","terdaftar","untuk","ml","60","adalah","agar ","alami","aman","anti","asli","besar","cocok","dan","dapat","dengan","di","digunakan","dijamin","halal","itu","jamu","kesehatan","kita","kondom","konsumen","lain","lama","lebih","lengkap","manjur","mengatasi","menjadi","merupakan ","mujarab","obat","oleh","oles","ori","original","pada","paling","paten","penyakit","permanen","pria","ready","resmi","ringan","sakit","segala","sehat","sehingga","seller","seperti","serbuk","tahan","terbuat","tidak","top","yang"]
tokenizer = RegexpTokenizer(r'\w+')

for i in range(1,gv_range):
    file = str(gv_folder)+'/'+str(gv_tipe_data)+'/'+str(i)+'.json'
    f = open(file)
    yourList = f.readlines()
    yourList = json.loads(yourList[0])
    lenyL = len(yourList)-1
    anarr = []
    itter = 0
    for p in yourList:
        if itter<lenyL: 
            jso = json.loads(p)
            wordsFiltered = []
            newtext = jso['title'].lower()
            newtext = tokenizer.tokenize(newtext)
            for w in newtext:
                if w not in stopWords:
                    wordsFiltered.append(w)
            newtext = ' '.join(map(str, wordsFiltered)) 
            jso['title'] = newtext
            anarr.append(jso)
        itter+=1 
    cek = pd.DataFrame(anarr)
    frames.append(cek)
    
ourData = pd.concat(frames)

print('1. Read all data...')


ourData.to_csv(str(gv_folder)+'/new_data_stopwrd_'+str(gv_tipe_data)+'_'+str(gv_dt_string)+'.csv',index=False)
newDf = ourData[ourData['sold'] > gv_sold]

print('2. Saved the merged data...')
print('3. Read the merged data...')

vect = TfidfVectorizer(use_idf=True,ngram_range=[3,3],min_df=2)
tfidf_matrix = vect.fit_transform(newDf['title'].apply(lambda x: np.str_(x)))
df = pd.DataFrame(tfidf_matrix.toarray(), columns = vect.get_feature_names()).T
df['mean'] = df.mean(axis=1)
df = df[['mean']]
df = df[df['mean']!=0.0]
df = df.sort_values(by=['mean'],ascending=False)

print('4. Calculate tfidf...')

newtigdata = df.head(50)
newtigdata = newtigdata.reset_index()
newtigdata.columns = ['keywords','mean_tf-idf']
remove = []
for z,wo in newtigdata.iterrows():
    if z not in remove:
        bef = [wo['keywords'],wo['mean_tf-idf']]
        befarr = tokenizer.tokenize(bef[0])
        for i,row in newtigdata.iterrows(): 
            if bef[0] != row['keywords']:
                curarr = tokenizer.tokenize(row['keywords'])
                diff = set(befarr) - set(curarr)
                if len(diff) <= 1:
                    if bef[1] > row['mean_tf-idf']:
                        remove.append(i)
                    elif bef[1] < row['mean_tf-idf']:
                        remove.append(i-1)
                    else:
                        beftext = ' '.join(map(str, befarr))
                        curtext = ' '.join(map(str, curarr))
                        if len(curtext) < len(beftext):
                            remove.append(i)
                        else:
                            remove.append(i-1) 
        

remove = set(remove)
remove = list(remove)
newtigdata = newtigdata.drop(remove)
newtigdata = newtigdata.reset_index(drop=True)
newtigdata = newtigdata.head(20)

print('5. Pattern matching removal...')


# -------------------------- calculation process ------------------

newtigdata['product_total_sold'] = 0
newtigdata['product_total_seen'] = 0
newtigdata['seller_bigest'] = 0
newtigdata['seller_total_seen'] = 0
newtigdata['seller_total_sold'] = 0
exceptret = 0
for i,row in newtigdata.iterrows():
    befarr = tokenizer.tokenize(row['keywords'])
    for z,wor in newDf.iterrows():
        try:
            curarr = tokenizer.tokenize(wor['title'])
            diff = set(befarr) - set(curarr)
            if len(diff) == 0:
                newtigdata.loc[i,'product_total_seen'] =  newtigdata.loc[i,'product_total_seen'] + wor['seen']
                newtigdata.loc[i,'product_total_sold'] =  newtigdata.loc[i,'product_total_sold'] + wor['sold']  
                if newtigdata.loc[i,'seller_total_sold'] < wor['sold']:
                    newtigdata.loc[i,'seller_total_seen'] =  wor['seen']
                    newtigdata.loc[i,'seller_total_sold'] =  wor['sold']
                    newtigdata.loc[i,'seller_bigest'] = wor['seller']
        except:
            exceptret+=1

print('6. Pattern matching the ads...')

newtigdata.to_csv(str(gv_folder)+'/result_analysis_'+str(gv_tipe_data)+'_'+str(gv_dt_string)+'.csv',index=False)
newtigdata.to_csv(gv_folder_frontend+'/'+gv_tipe_data+'.csv',index=False)

print('7. Saved result data...')
print('Done.')