const gv = {
  heightBlock: 0,
  numDataShow: 5,
  folderData: "data_2019_10_17",
  updated: "2019/10/17 10:30:12",
  allData: {}
};
const qNum = d3.format(",d");

$(document).ready(async function() {
  $(".updated-date-info").html("Last updated: " + gv.updated);
  let blockName = ["Obat", "OTSK", "Pangan", "Kosmetik"];
  await blockName.forEach(async function(x) {
    await $("#content").append(render(x));
    await $("body").append(renderModal(x));
    if (!gv.heightBlock) {
      gv.heightBlock =
        $(".block-mod").height() -
        $(".block-header-mod").height() -
        $(".block-header-boxvis").height() * 3;
    }
  });
  $(".detail-button").on("click", function() {
    id = $(this)
      .attr("id")
      .replace("more-", "modal-");
    $("#" + id).modal("show");
  });
  initData("obat");
  initData("otsk");
  initData("pangan");
  initData("kosmetik");
});

function drawDataTable(data, id) {
  let visdata = [];
  data.forEach(function(d, i) {
    visdata.push({
      keywords: d["keywords"],
      weight: parseFloat(d["mean_tf-idf"]).toFixed(5),
      product_total_seen: qNum(d["product_total_seen"]),
      product_total_sold: qNum(d["product_total_sold"]),
      bigest_seller: d["seller_bigest"],
      seller_total_seen: qNum(d["seller_total_seen"]),
      seller_total_sold: qNum(d["seller_total_sold"])
    });
  });
  let cols = [];
  for (x in visdata[0]) {
    cols.push({
      title: x.split("_").join(" "),
      data: x
    });
  }
  $("#tabular-" + id).DataTable({
    data: visdata,
    columns: cols,
    order: [[1, "desc"]]
  });
}

async function initData(param) {
  let dataCalled;
  await d3.csv(gv.folderData + "/" + param + ".csv").then(function(data) {
    dataCalled = data;
    drawDataTable(data, param);
  });
  let keywordsVisData = theData => {
    let a = [],
      b = [];
    theData.forEach(function(d, i) {
      if (i < gv.numDataShow * 2) {
        a.push(d.keywords);
        b.push(parseFloat(d["mean_tf-idf"]).toFixed(2));
      }
    });
    return [a, b];
  };

  let productVisData = async theData => {
    let a = [],
      b = [],
      c = [];
    await theData.sort(function(a, b) {
      return parseInt(a.product_total_sold) > parseInt(b.product_total_sold)
        ? -1
        : 1;
    });
    await theData.forEach(function(d, i) {
      if (i < gv.numDataShow) {
        a.push(d.keywords);
        b.push(parseInt(d.product_total_sold));
        c.push(parseInt(d.product_total_seen));
      }
    });
    return [a, b, c];
  };

  let sellerVisData = async theData => {
    let a = [],
      b = [],
      c = [];
    await theData.sort(function(a, b) {
      return parseInt(a.product_total_sold) > parseInt(b.product_total_sold)
        ? -1
        : 1;
    });
    await theData.forEach(function(d, i) {
      if (i < gv.numDataShow) {
        a.push(d.seller_bigest);
        b.push(parseInt(d.seller_total_sold));
        c.push(parseInt(d.seller_total_seen));
      }
    });
    return [a, b, c];
  };

  drawKeywords("kwd-" + param, keywordsVisData(dataCalled));
  productVisData(dataCalled).then(function(resolve) {
    drawProduct("prod-" + param, resolve);
  });
  sellerVisData(dataCalled).then(function(resolve) {
    sellerSeenSold("sel-" + param, resolve);
  });
}

function drawKeywords(id, data) {
  const options = {
    chart: {
      toolbar: {
        show: false
      },
      height: gv.heightBlock / 3,
      type: "bar"
    },
    plotOptions: {
      bar: {
        columnWidth: "50%"
      }
    },
    dataLabels: {
      enabled: false,
      textAnchor: "middle",
      style: {
        colors: ["#fff"],
        fontSize: "8px"
      },
      formatter: function(val, opt) {
        return qNum(val);
      },
      offsetY: 20,
      dropShadow: {
        enabled: true
      }
    },
    series: [
      {
        name: "Total",
        data: data[1]
      }
    ],
    colors: ["#007afe"],
    xaxis: {
      labels: {
        rotate: -45,
        style: {
          fontSize: "0.6rem"
        }
      },
      categories: data[0]
    },
    yaxis: {
      show: true,
      title: {
        text: "Weight"
      }
    }
  };

  const chart = new ApexCharts(document.querySelector("#" + id), options);

  chart.render();
}

function drawProduct(id, data) {
  var options = {
    chart: {
      height: gv.heightBlock / 3,
      type: "bar",
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: {
          position: "top"
        }
      }
    },
    dataLabels: {
      enabled: false,
      offsetX: -6,
      style: {
        fontSize: "12px",
        colors: ["#fff"]
      }
    },
    stroke: {
      show: true,
      width: 1,
      colors: ["#fff"]
    },
    series: [
      {
        name: "Sold",
        data: data[1]
      },
      {
        name: "Seen",
        data: data[2]
      }
    ],
    colors: ["#17a0b7", "#6c757d"],
    legend: {
      show: false
    },
    xaxis: {
      categories: data[0]
    }
  };

  var chart = new ApexCharts(document.querySelector("#" + id), options);

  chart.render();
}

function sellerSeenSold(id, data) {
  const options = {
    chart: {
      height: gv.heightBlock / 3,
      type: "bar",
      stacked: true,
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      bar: {
        horizontal: true
      }
    },
    stroke: {
      width: 1,
      colors: ["#fff"]
    },
    series: [
      {
        name: "Sold",
        data: data[1]
      },
      {
        name: "Seen",
        data: data[2]
      }
    ],
    colors: ["#17a0b7", "#6c757d"],
    xaxis: {
      categories: data[0],
      labels: {
        formatter: function(val) {
          return val + "K";
        }
      }
    },
    yaxis: {
      title: {
        text: undefined
      }
    },
    tooltip: {
      y: {
        formatter: function(val) {
          return val + "K";
        }
      }
    },
    fill: {
      opacity: 1
    },

    legend: {
      show: false
    },
    dataLabels: {
      enabled: true,
      textAnchor: "end",
      style: {
        colors: ["#fff"],
        fontSize: "8px"
      },
      formatter: function(val, opt) {
        return qNum(val);
      },
      dropShadow: {
        enabled: true
      }
    }
  };

  const chart = new ApexCharts(document.querySelector("#" + id), options);

  chart.render();
}

function render(name) {
  let header;
  name == "Pangan" ? (header = "Pangan Olahan") : (header = name);
  let htm =
    '<div class="col-md-3 block-div pl-3">' +
    '<div class="col-md-12 block-mod">' +
    '<div class="row">' +
    '<div class="col-md-12 m-0 p-0">' +
    '<div class="row m-0">' +
    '<div class="col-md-6 block-header-mod bg-danger">' +
    header +
    "</div>" +
    '<div class="col-md-6 p-0">' +
    '<div class="col-md-12 block-header-info bg-secondary">' +
    '<span class="detail-button" id="more-' +
    name.toLowerCase() +
    '"><i class="fa fa-table" aria-hidden="true"></i> More detail</span>' +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="col-md-12 a-block-vis p-0">' +
    '<div class="col-md-12 block-header-boxvis">' +
    '<div class="row">' +
    '<div class="col-md-6 p-0">' +
    '<span class="block-title p-2">Keywords</span>' +
    "</div>" +
    '<div class="col-md-6">' +
    '<span class="block-note p-0 float-right"><i class="fa fa-stop weightblock"></i> Weighting similarity Ads</span>' +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="col-md-12 block-kwd p-0" id="kwd-' +
    name.toLowerCase() +
    '"></div>' +
    "</div>" +
    '<div class="col-md-12 a-block-vis p-0 block-bottom">' +
    '<div class="col-md-12 block-header-boxvis">' +
    '<div class="row">' +
    '<div class="col-md-6 p-0">' +
    '<span class="block-title p-2">Products</span>' +
    "</div>" +
    '<div class="col-md-6">' +
    '<span class="block-note p-0 float-right"><i class="fa fa-stop soldblock"></i> Sold | <i class="fa fa-stop seenblock"></i> Seen</span>' +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="col-md-12 block-kwd prodvis p-0" id="prod-' +
    name.toLowerCase() +
    '"></div>' +
    "</div>" +
    '<div class="col-md-12 a-block-vis p-0 block-bottom">' +
    '<div class="col-md-12 block-header-boxvis">' +
    '<div class="row">' +
    '<div class="col-md-6 p-0">' +
    '<span class="block-title p-2">Sellers</span>' +
    "</div>" +
    '<div class="col-md-6">' +
    '<span class="block-note p-0 float-right"><i class="fa fa-stop soldblock"></i> Sold | <i class="fa fa-stop seenblock"></i> Seen</span>' +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="col-md-12 block-kwd selvis p-0" id="sel-' +
    name.toLowerCase() +
    '"></div>' +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
  return htm;
}

function renderModal(name) {
  let htm =
    '<div class="modal" id="modal-' +
    name.toLowerCase() +
    '" tabindex="-1" role="dialog">' +
    '<div class="modal-dialog modal-xl" role="document">' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<h5 class="modal-title" id="title-modal-' +
    name.toLowerCase() +
    '">' +
    name +
    "</h5>" +
    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    "</button>" +
    "</div>" +
    '<div class="modal-body" id="content-modal-' +
    name.toLowerCase() +
    '">' +
    '<table class="table table-striped table-bordered" id="tabular-' +
    name.toLowerCase() +
    '" style="width:100%"></table>' +
    "</div>" +
    '<div class="modal-footer">' +
    '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
  return htm;
}
